package com.msr;

import org.springframework.boot.SpringApplication;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
public class TestBuildingsApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(BuildingsApiApplication.class, args);
    }
}
