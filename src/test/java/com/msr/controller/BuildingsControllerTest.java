package com.msr.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

/**
 * Intended as a starting point for Unit Testing of the BuildingsController class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class BuildingsControllerTest {

    private BuildingsController buildingsController;

    @Before
    public void setUp() {
        buildingsController = new BuildingsController(null);
    }

    @Test
    public void testSampleResponse_NullMessageParameter() throws Exception {
        String response = buildingsController.getSampleResponse(null, false);
        assertEquals(BuildingsController.NO_SAMPLE_PARAM_PROVIDED, response);
    }

    @Test
    public void testSampleResponse_EmptyMessageParameter() throws Exception {
        String response = buildingsController.getSampleResponse("", false);
        assertEquals(BuildingsController.NO_SAMPLE_PARAM_PROVIDED, response);
    }

    @Test
    public void testSampleResponse_MessageParameterProvided() throws Exception {
        String expectedString = "This is the expected output parameter.";
        String response = buildingsController.getSampleResponse(expectedString, false);
        assertEquals(BuildingsController.SAMPLE_PARAM_PROVIDED + expectedString, response);
    }

    @Test(expected = Exception.class)
    public void testSampleResponse_ErrorThrown_WithoutMessageParameter() throws Exception {
        buildingsController.getSampleResponse(null, true);
    }

    @Test(expected = Exception.class)
    public void testSampleResponse_ErrorThrown_WithMessageParameter() throws Exception {
        buildingsController.getSampleResponse("test", true);
    }
}