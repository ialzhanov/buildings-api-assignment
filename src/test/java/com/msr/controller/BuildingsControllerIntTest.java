package com.msr.controller;

import com.msr.dto.SiteRequestDto;
import com.msr.dto.SiteResponseDto;
import com.msr.dto.SiteUseRequestDto;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import com.msr.repository.SiteRepository;
import com.msr.repository.SiteUseRepository;
import com.msr.repository.UseTypesRepository;
import com.msr.service.SiteService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.CREATED;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class BuildingsControllerIntTest {

    private static final String OFFICE_USE_TYPE = "Office";
    private static final String ZOO_USE_TYPE = "Zoo";
    private static final String STADIUM_USE_TYPE = "Stadium";
    private static final String MSR_DATA_CENTER_USE_DESCRIPTION = "MSR Data Center";
    private static final String MSR_OFFICE_SUITE_1_USE_DESCRIPTION = "MSR Office - Suite 1";
    private static final String MSR_OFFICE_SUITE_2_USE_DESCRIPTION = "MSR Office - Suite 2";
    private static final String MSR_OFFICE_SUITE_3_USE_DESCRIPTION = "MSR Office - Suite 3";

    private BuildingsController buildingsController;

    @Before
    public void setUp() {
        buildingsController = new BuildingsController(new SiteService(siteRepository, useTypesRepository));
    }

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private UseTypesRepository useTypesRepository;

    @Autowired
    private SiteUseRepository siteUseRepository;

    /**
     * Intended to test the controller's get all sites with usages.
     */
    @Test
    public void test_get_all_sites_with_usages() {
        UseType officeUseType = createUseType(OFFICE_USE_TYPE);
        UseType zooUseType = createUseType(ZOO_USE_TYPE);
        UseType stadiumUseType = createUseType(STADIUM_USE_TYPE);

        Site site = getTestSite();
        site.getSiteUses().add(createSiteUse(
                null, MSR_DATA_CENTER_USE_DESCRIPTION, site, new BigDecimal("8000"), stadiumUseType));
        site.getSiteUses().add(createSiteUse(
                null, MSR_OFFICE_SUITE_1_USE_DESCRIPTION, site, new BigDecimal("3001"), officeUseType));
        site.getSiteUses().add(createSiteUse(
                null, MSR_OFFICE_SUITE_2_USE_DESCRIPTION, site, new BigDecimal("3002"), officeUseType));
        site.getSiteUses().add(createSiteUse(
                null, MSR_OFFICE_SUITE_3_USE_DESCRIPTION, site, new BigDecimal("3003"), officeUseType));

        Site persisted = siteRepository.save(site);
        List<SiteResponseDto> sites = buildingsController.get();
        assertEquals(1, sites.size());
        assertEquals(persisted.getId(), sites.get(0).getId());
        assertEquals(persisted.getName(), sites.get(0).getName());
        assertEquals(persisted.getAddress(), sites.get(0).getAddress());
        assertEquals(persisted.getCity(), sites.get(0).getCity());
        assertEquals(persisted.getState(), sites.get(0).getState());
        assertEquals(persisted.getZipCode(), sites.get(0).getZipCode());
        assertEquals(officeUseType.getId(), sites.get(0).getPrimaryType().getId());
        assertEquals(officeUseType.getName(), sites.get(0).getPrimaryType().getName());
        assertEquals(new BigDecimal("9006"), sites.get(0).getTotalSize());
    }

    /**
     * Intended to test the controller's get all sites functionality.
     */
    @Test
    public void test_get_all_sites_without_any_usages() {
        Site persisted = siteRepository.save(getTestSite());
        List<SiteResponseDto> sites = buildingsController.get();
        assertEquals(1, sites.size());
        assertEquals(persisted.getId(), sites.get(0).getId());
        assertEquals(persisted.getName(), sites.get(0).getName());
        assertEquals(persisted.getAddress(), sites.get(0).getAddress());
        assertEquals(persisted.getCity(), sites.get(0).getCity());
        assertEquals(persisted.getState(), sites.get(0).getState());
        assertEquals(persisted.getZipCode(), sites.get(0).getZipCode());
    }

    /**
     * Intended to test the controller's get site by ID functionality.
     */
    @Test
    public void test_get_site_by_id() {
        Site persisted = siteRepository.save(getTestSite());
        List<SiteResponseDto> sites = buildingsController.get();
        assertEquals(1, sites.size());
        assertEquals(persisted.getId(), sites.get(0).getId());
        assertEquals(persisted.getName(), sites.get(0).getName());
        assertEquals(persisted.getAddress(), sites.get(0).getAddress());
        assertEquals(persisted.getCity(), sites.get(0).getCity());
        assertEquals(persisted.getState(), sites.get(0).getState());
        assertEquals(persisted.getZipCode(), sites.get(0).getZipCode());
    }

    @Test
    public void test_save_site() {
        UseType officeUseType = createUseType(OFFICE_USE_TYPE);
        UseType zooUseType = createUseType(ZOO_USE_TYPE);
        UseType stadiumUseType = createUseType(STADIUM_USE_TYPE);

        SiteRequestDto siteRequestDto = new SiteRequestDto();
        siteRequestDto.setName("Measurabl HQ");
        siteRequestDto.setAddress("707 Broadway Suite 1000");
        siteRequestDto.setCity("San Diego");
        siteRequestDto.setState("CA");
        siteRequestDto.setZipCode("92101");
        siteRequestDto.getSiteUses().add(createSiteUseRequestDto(null, stadiumUseType.getName(),
                MSR_DATA_CENTER_USE_DESCRIPTION, new BigDecimal("8000")));
        siteRequestDto.getSiteUses().add(createSiteUseRequestDto(null, officeUseType.getName(),
                MSR_OFFICE_SUITE_1_USE_DESCRIPTION, new BigDecimal("3001")));
        siteRequestDto.getSiteUses().add(createSiteUseRequestDto(null, zooUseType.getName(),
                MSR_OFFICE_SUITE_2_USE_DESCRIPTION, new BigDecimal("3002")));

        ResponseEntity<Integer> responseEntity = buildingsController.save(siteRequestDto);
        assertEquals(CREATED, responseEntity.getStatusCode());
        Integer id = responseEntity.getBody();
        assertNotNull(id);

        Optional<Site> optionalPersisted = siteRepository.findById(id);
        assertTrue(optionalPersisted.isPresent());

        Site persisted = optionalPersisted.get();

        assertEquals(persisted.getId(), id);
        assertEquals(persisted.getName(), siteRequestDto.getName());
        assertEquals(persisted.getAddress(), siteRequestDto.getAddress());
        assertEquals(persisted.getCity(), siteRequestDto.getCity());
        assertEquals(persisted.getState(), siteRequestDto.getState());
        assertEquals(persisted.getZipCode(), siteRequestDto.getZipCode());

        BigDecimal totalSqft;
        String primaryUseTypeName;

        Map<Integer, BigDecimal> sumMap = new HashMap<>();
        Map<Integer, String> nameMap = new HashMap<>();
        for (SiteUse su : persisted.getSiteUses()) {
            id = su.getUseType().getId();
            if (!sumMap.containsKey(id)) {
                sumMap.put(id, BigDecimal.ZERO);
                nameMap.put(id, su.getUseType().getName());
            }
            sumMap.put(id, sumMap.get(id).add(su.getSizeSqft()));
        }

        assertEquals(3, sumMap.size());
        assertEquals(3, nameMap.size());

        Map.Entry<Integer, BigDecimal> max = null;
        for (Map.Entry<Integer, BigDecimal> entry : sumMap.entrySet()) {
            if (max == null) {
                max = entry;
            } else {
                if (max.getValue().compareTo(entry.getValue()) < 0) {
                    max = entry;
                }
            }
        }

        assertNotNull(max);

        totalSqft = max.getValue();
        primaryUseTypeName = nameMap.get(max.getKey());

        assertEquals(stadiumUseType.getName(), primaryUseTypeName);
        assertEquals(new BigDecimal("8000"), totalSqft);
    }

    @Test
    @Ignore
    public void test_update_site_by_id() {
        /* TBA */
    }

    @Test
    @Ignore
    public void test_replace_site_by_id() {
        /* TBA */
    }

    @Test
    @Ignore
    public void test_delete_site_by_id() {
        /* TBA */
    }

    @Test
    @Ignore
    public void test_get_site_uses_by_id() {
        /* TBA */
    }

    @Test
    @Ignore
    public void test_save_site_uses_by_id() {
        /* TBA */
    }

    private Site getTestSite() {
        Site testSite = new Site();
        testSite.setName("Measurabl HQ");
        testSite.setAddress("707 Broadway Suite 1000");
        testSite.setCity("San Diego");
        testSite.setState("CA");
        testSite.setZipCode("92101");
        return testSite;
    }

    private UseType createUseType(String name) {
        UseType useType = new UseType();
        useType.setName(name);
        return useType;
    }

    private SiteUse createSiteUse(Integer id, String description, Site site, BigDecimal sizeSqft, UseType useType) {
        SiteUse siteUse = new SiteUse();
        siteUse.setId(id);
        siteUse.setDescription(description);
        siteUse.setSite(site);
        siteUse.setSizeSqft(sizeSqft);
        siteUse.setUseType(useType);
        return siteUse;
    }

    private SiteUseRequestDto createSiteUseRequestDto(Integer useTypeId,
                                                      String useName,
                                                      String useDescription,
                                                      BigDecimal sizeSqft) {
        SiteUseRequestDto siteUseRequestDto = new SiteUseRequestDto();
        siteUseRequestDto.setUseTypeId(useTypeId);
        siteUseRequestDto.setUseName(useName);
        siteUseRequestDto.setUseDescription(useDescription);
        siteUseRequestDto.setSizeSqft(sizeSqft);
        return siteUseRequestDto;
    }
}
