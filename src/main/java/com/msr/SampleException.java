package com.msr;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
public class SampleException extends Exception {

    public SampleException(String sampleExceptionMessage) {
        super(sampleExceptionMessage);
    }
}
