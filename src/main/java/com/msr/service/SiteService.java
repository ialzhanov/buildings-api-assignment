package com.msr.service;

import com.msr.dto.SiteRequestDto;
import com.msr.dto.SiteResponseDto;
import com.msr.dto.SiteUseRequestDto;
import com.msr.dto.UseTypeRequestDto;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import com.msr.repository.SiteRepository;
import com.msr.repository.UseTypesRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@Service
public class SiteService {

    private SiteRepository siteRepository;
    private UseTypesRepository useTypesRepository;

    public SiteService(SiteRepository siteRepository, UseTypesRepository useTypesRepository) {
        this.siteRepository = siteRepository;
        this.useTypesRepository = useTypesRepository;
    }

    /**
     * @param dto represents a site
     */
    @Transactional(rollbackOn = Exception.class)
    public Site saveSite(SiteRequestDto dto) {
        return siteRepository.save(convertRequestDtoToModel(dto));
    }

    public List<SiteResponseDto> findAllSites() {
        List<SiteResponseDto> siteResponseDtoList = new LinkedList<>();
        siteRepository.findAll().forEach(site -> siteResponseDtoList.add(convertModelToResponseDto(site)));
        return siteResponseDtoList;
    }

    public SiteResponseDto findSiteById(int id) {
        return siteRepository.findById(id).map(this::convertModelToResponseDto).orElse(null);
    }

    public List<UseTypeRequestDto> findAllUseTypes() {
        List<UseTypeRequestDto> list = new ArrayList<>();
        UseTypeRequestDto useTypeRequestDto;
        for (UseType useType : useTypesRepository.findAll()) {
            useTypeRequestDto = new UseTypeRequestDto();
            useTypeRequestDto.setId(useType.getId());
            useTypeRequestDto.setName(useType.getName());
            list.add(useTypeRequestDto);
        }
        return list;
    }

    @Transactional(rollbackOn = Exception.class)
    public void saveUseType(UseTypeRequestDto useTypeRequestDto) {
        UseType useType = new UseType();
        useType.setName(useTypeRequestDto.getName());
        useTypesRepository.save(useType);
    }

    private SiteResponseDto convertModelToResponseDto(Site site) {
        Map<Integer, BigDecimal> maps = new HashMap<>();
        Map<Integer, String> useNameMap = new HashMap<>();
        SiteResponseDto siteResponseDto = new SiteResponseDto();
        siteResponseDto.setId(site.getId());
        siteResponseDto.setName(site.getName());
        siteResponseDto.setAddress(site.getAddress());
        siteResponseDto.setCity(site.getCity());
        siteResponseDto.setState(site.getState());
        siteResponseDto.setZipCode(site.getZipCode());
        preProcess(site, maps, useNameMap);
        assignPrimaryTypeAndTotalSqft(siteResponseDto, maps, useNameMap);
        return siteResponseDto;
    }

    /**
     * Calculate sums of sqft per site use type and prepare a look up map for id(key) : description dictionary
     *
     * @param site       site
     * @param maps       map of sqft sums per use type identifier
     * @param useNameMap use name mapped by use type identifier
     */
    private void preProcess(Site site, Map<Integer, BigDecimal> maps, Map<Integer, String> useNameMap) {
        Integer id;
        for (SiteUse su : site.getSiteUses()) {
            id = su.getUseType().getId();
            if (!maps.containsKey(id)) {
                maps.put(id, BigDecimal.ZERO);
                useNameMap.put(id, su.getUseType().getName());
            }
            maps.put(id, maps.get(id).add(su.getSizeSqft()));
        }
    }

    /**
     * @param siteResponseDto dto to return as response
     * @param maps            map of sqft sums per use type identifier
     * @param useNameMap      use name mapped by use type identifier
     */
    private void assignPrimaryTypeAndTotalSqft(SiteResponseDto siteResponseDto,
                                               Map<Integer, BigDecimal> maps, Map<Integer, String> useNameMap) {
        if (!maps.isEmpty()) {
            Map.Entry<Integer, BigDecimal> max = null;
            for (Map.Entry<Integer, BigDecimal> entry : maps.entrySet()) {
                if (max == null) {
                    max = entry;
                } else {
                    if (max.getValue().compareTo(entry.getValue()) < 0) {
                        max = entry;
                    }
                }
            }
            if (max != null) {
                siteResponseDto.setTotalSize(max.getValue());
                UseTypeRequestDto useTypeRequestDto = new UseTypeRequestDto();
                useTypeRequestDto.setId(max.getKey());
                useTypeRequestDto.setName(useNameMap.get(max.getKey()));
                siteResponseDto.setPrimaryType(useTypeRequestDto);
            }
        }
    }

    private Site convertRequestDtoToModel(SiteRequestDto siteDto) {
        Site site = new Site();
        site.setName(siteDto.getName());
        site.setAddress(siteDto.getAddress());
        site.setCity(siteDto.getCity());
        site.setState(siteDto.getState());
        site.setZipCode(siteDto.getZipCode());

        UseType useType;
        SiteUse siteUse;
        Set<SiteUse> set = new HashSet<>();
        for (SiteUseRequestDto usesDto : siteDto.getSiteUses()) {
            useType = new UseType();
            useType.setId(usesDto.getUseTypeId());
            useType.setName(usesDto.getUseName());
            siteUse = new SiteUse();
            siteUse.setDescription(usesDto.getUseDescription());
            siteUse.setSizeSqft(usesDto.getSizeSqft());
            siteUse.setUseType(useType);
            set.add(siteUse);
        }
        site.setSiteUses(set);
        return site;
    }
}
    