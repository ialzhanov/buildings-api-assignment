package com.msr.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@Data
public class UseTypeRequestDto {

    /**
     *
     */
    private Integer id;

    /**
     *
     */
    @NotNull
    private String name;
}
