package com.msr.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@Data
public class SiteResponseDto {

    /**
     *
     */
    private Integer id;
    /**
     *
     */
    private String name;

    /**
     *
     */
    private String address;

    /**
     *
     */
    private String city;

    /**
     *
     */
    private String state;

    /**
     *
     */
    @JsonProperty("zipcode")
    private String zipCode;

    /**
     *
     */
    @JsonProperty("total_size")
    private BigDecimal totalSize;

    /**
     *
     */
    @JsonProperty("primary_type")
    private UseTypeRequestDto primaryType;
}
