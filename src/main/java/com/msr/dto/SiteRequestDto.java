package com.msr.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@Getter
@Setter
public class SiteRequestDto {

    /**
     *
     */
    private Integer id;

    /**
     *
     */
    @NotNull
    @Size(max = 32)
    private String name;

    /**
     *
     */
    @NotNull
    @Size(max = 250)
    private String address;

    /**
     *
     */
    @NotNull
    @Size(max = 64)
    private String city;

    /**
     *
     */
    @NotNull
    @Size(max = 32)
    private String state;

    /**
     *
     */
    @NotNull
    @Size(max = 5)
    @JsonProperty("zipcode")
    private String zipCode;

    /**
     *
     */
    private List<SiteUseRequestDto> siteUses = new ArrayList<>();
}
