package com.msr.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@Getter
@Setter
public class SiteUseRequestDto {

    /**
     * identifier
     */
    private Integer useTypeId;

    /**
     * Bank Branch
     */
    @NotNull
    private String useName;

    /**
     * MSR Office - Suite 2
     */
    @NotNull
    private String useDescription;

    /**
     * size in square feet
     */
    @NotNull
    private BigDecimal sizeSqft;
}
