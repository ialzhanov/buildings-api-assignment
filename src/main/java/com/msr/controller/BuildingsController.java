package com.msr.controller;

import com.msr.SampleException;
import com.msr.dto.SiteRequestDto;
import com.msr.dto.SiteResponseDto;
import com.msr.dto.SiteUseRequestDto;
import com.msr.model.Site;
import com.msr.service.SiteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

/**
 * Respond to site requests
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/buildings")
public class BuildingsController {

    /* Sample Output messages. */
    private static final String SAMPLE_RESPONSE_BASE = "This is a sample response to test if your BuildingController is responding appropriately.  ";
    static final String SAMPLE_PARAM_PROVIDED = SAMPLE_RESPONSE_BASE + "The request param you passed was: ";
    static final String NO_SAMPLE_PARAM_PROVIDED = SAMPLE_RESPONSE_BASE + "No request param was provided.";
    static final String SAMPLE_EXCEPTION_MESSAGE = SAMPLE_RESPONSE_BASE + "An expected error was thrown.";

    private SiteService siteService;

    public BuildingsController(SiteService siteService) {
        this.siteService = siteService;
    }

    /**
     * @return list of available sites
     */
    @GetMapping
    public List<SiteResponseDto> get() {
        return siteService.findAllSites();
    }

    /**
     * @param id site unique identifier
     * @return a site associated with the specified id
     */
    @GetMapping("/{id}")
    public ResponseEntity<SiteResponseDto> getById(@PathVariable @NotNull Integer id) {
        SiteResponseDto site = siteService.findSiteById(id);
        return site == null ? ResponseEntity.notFound().build() : ResponseEntity.status(OK).body(site);
    }

    /**
     * @param siteDto represents a site
     * @return HttpStatus.CREATED
     */
    @PostMapping
    public ResponseEntity<Integer> save(@Valid @RequestBody SiteRequestDto siteDto) {
        Site s = siteService.saveSite(siteDto);
        return ResponseEntity.status(CREATED).body(s.getId());
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteAll() {
        return ResponseEntity.status(NOT_IMPLEMENTED).build();
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Void> updateSiteById(@PathVariable @NotNull Integer id,
                                               @RequestBody SiteRequestDto siteDto) {
        return ResponseEntity.status(NOT_IMPLEMENTED).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> replaceSiteById(@PathVariable @NotNull Integer id,
                                                @RequestBody SiteRequestDto siteDto) {
        return ResponseEntity.status(NOT_IMPLEMENTED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSiteById(@PathVariable @NotNull Integer id) {
        return ResponseEntity.status(NOT_IMPLEMENTED).build();
    }

    @GetMapping("/{id}/uses")
    public ResponseEntity<Void> getSiteUses(@PathVariable @NotNull Integer id) {
        return ResponseEntity.status(NOT_IMPLEMENTED).build();
    }

    @PostMapping("/{id}/uses")
    public ResponseEntity<Void> saveSiteUses(@PathVariable @NotNull Integer id,
                                             @Valid @RequestBody SiteUseRequestDto siteDto) {
        return ResponseEntity.status(NOT_IMPLEMENTED).build();
    }

    /**
     * Used simply to check if your BuildingsController is responding to requests.
     * Has no function other than echoing.
     *
     * @return A sample message based on the input parameters.
     * @throws SampleException Only when 'throwError' is true.
     */
    @ApiOperation("Returns a sample message for baseline controller testing.")
    @GetMapping("/sample")
    public String getSampleResponse(@ApiParam("The message that will be echoed back to the user.")
                                    @RequestParam(required = false) final String message,
                                    @ApiParam("Forces this endpoint to throw a generic error.")
                                    @RequestParam(required = false) final boolean throwError) throws SampleException {
        String response;
        if (throwError) {
            throw new SampleException(SAMPLE_EXCEPTION_MESSAGE);
        } else if (StringUtils.isEmpty(message)) {
            response = NO_SAMPLE_PARAM_PROVIDED;
        } else {
            response = SAMPLE_PARAM_PROVIDED + message;
        }
        return response;
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    