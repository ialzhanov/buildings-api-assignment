package com.msr.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Site uses POJO
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Getter
@Setter
@Entity(name = "SITE_USE")
public class SiteUse implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "DESC")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SITE_ID")
    private Site site;

    @Column(name = "SIZE_SQFT")
    private BigDecimal sizeSqft;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "USE_TYPE_ID")
    private UseType useType;

    @Override
    public String toString() {
        return "SiteUse{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", sizeSqft=" + sizeSqft +
                ", useType=" + useType +
                '}';
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    