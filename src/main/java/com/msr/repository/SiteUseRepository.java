package com.msr.repository;

import com.msr.model.SiteUse;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
public interface SiteUseRepository extends JpaRepository<SiteUse, Integer> {
}
