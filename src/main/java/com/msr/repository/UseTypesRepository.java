package com.msr.repository;

import com.msr.model.UseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository functionality for UseTypes
 *
 * @author Measurabl
 * @since 2019-06-12
 */
public interface UseTypesRepository extends JpaRepository<UseType, Integer> {

    @Query("select ut from USE_TYPE ut where ut.name = :name")
    UseType findByName(String name);
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    