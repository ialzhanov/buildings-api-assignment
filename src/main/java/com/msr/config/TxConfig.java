package com.msr.config;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author ialzhanov
 * @since 1/15/2020
 */
@Configuration
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public class TxConfig {

}
